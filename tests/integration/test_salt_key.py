def test_salt_key(master_minion, salt_fixt):
    """
    Test running salt-key -L
    """
    ret = salt_fixt.salt_key(["-L"])
    assert "pkg_tests" in ret["stdout"]
