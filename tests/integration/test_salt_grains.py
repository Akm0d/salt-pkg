def test_grains_items(master_minion, salt_fixt):
    """
    Test grains.items
    """
    ret = salt_fixt.salt_minion(["grains.items"])
    assert "osrelease" in ret["stdout"]


def test_grains_item_os(master_minion, salt_fixt):
    """
    Test grains.item os
    """
    ret = salt_fixt.salt_minion(["grains.item", "os"])
    assert "os" in ret["stdout"]


def test_grains_item_pythonversion(master_minion, salt_fixt):
    """
    Test grains.item pythonversion
    """
    ret = salt_fixt.salt_minion(["grains.item", "pythonversion"])
    assert "pythonversion" in ret["stdout"]


def test_grains_setval_key_val(master_minion, salt_fixt):
    """
    Test grains.setval key val
    """
    ret = salt_fixt.salt_minion(["grains.setval", "key", "val"])
    assert "key" in ret["stdout"]
