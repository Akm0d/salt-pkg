import tests.support.helpers


def test_salt_version(version, salt_fixt):
    """
    Test version outputed from salt --version
    """
    ret = tests.support.helpers.run(salt_fixt.salt_bin["salt"] + ["--version"])
    assert ret["stdout"].strip() == f"salt {version}"


def test_salt_versions_report_master(salt_fixt):
    """
    Test running --versions-report on master
    """
    ret = salt_fixt.salt_master(["--versions-report"])
    assert "Salt Version:" in ret["stdout"]


def test_salt_versions_report_minion(master_minion, salt_fixt):
    """
    Test running test.versions_report on minion
    """
    ret = salt_fixt.salt_minion(["test.versions_report"])
    assert "Salt Version:" in ret["stdout"]
